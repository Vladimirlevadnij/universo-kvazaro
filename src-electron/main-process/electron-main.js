import { app, BrowserWindow, nativeTheme } from 'electron'
const exec = require('child_process').exec

try {
  if (process.platform === 'win32' && nativeTheme.shouldUseDarkColors === true) {
    require('fs').unlinkSync(require('path').join(app.getPath('userData'), 'DevTools Extensions'))
  }
} catch (_) { }

/**
 * Set `__statics` path to static files in production;
 * The reason we are setting it here is that the path needs to be evaluated at runtime
 */
if (process.env.PROD) {
  global.__statics = __dirname
}

let mainWindow

function startBackend () {
  if (!process.env.DEV) {
    exec('server\\start.bat', (err, stdout, stderr) => {
      if (err) {
        console.error(err)
      }

      console.log(stdout)
    })
  }
}

function stopBackend () {
  if (!process.env.DEV) {
    exec('server\\stop.bat', (err, stdout, stderr) => {
      if (err) {
        console.error(err)
      }

      console.log(stdout)
    })
  }
}

function createWindow () {
  startBackend()
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    width: 1000,
    height: 600,
    useContentSize: true,
    webPreferences: {
      // Change from /quasar.conf.js > electron > nodeIntegration;
      // More info: https://quasar.dev/quasar-cli/developing-electron-apps/node-integration
      nodeIntegration: process.env.QUASAR_NODE_INTEGRATION,
      nodeIntegrationInWorker: process.env.QUASAR_NODE_INTEGRATION,

      // More info: /quasar-cli/developing-electron-apps/electron-preload-script
      // preload: path.resolve(__dirname, 'electron-preload.js')
      webSecurity: false,
      allowRunningInsecureContent: true
    }
  })

  mainWindow.setMenu(null)

  mainWindow.loadURL(process.env.APP_URL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.commandLine.appendSwitch('disable-features', 'OutOfBlinkCors')

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  stopBackend()
  if (process.platform !== 'darwin') {
    // delay before quit
    setTimeout(() => {
      app.quit()
    }, 3000)
    // app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})
