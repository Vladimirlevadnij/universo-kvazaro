import Vue from 'vue'
import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: 'http://127.0.0.1:8000/api/v1.0/',
  timeout: 3000,
  withCredentials: true,
  xsrfHeaderName: 'X-CSRFTOKEN',
  xsrfCookieName: 'csrftoken'
})

axiosInstance.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

Vue.prototype.$axios = axiosInstance
