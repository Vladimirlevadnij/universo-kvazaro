
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Start.vue') },
      { path: 'index', name: 'index', component: () => import('pages/Index.vue') },
      { path: 'view', name: 'view', component: () => import('pages/View.vue') },
      { path: 'about', name: 'about', component: () => import('pages/About.vue') }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    name: 'error',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
